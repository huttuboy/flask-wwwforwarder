from flask import Flask, redirect
from os import getenv


app = Flask(__name__)


@app.route('/')
def forwarder():
    return redirect(getenv("FORWARD_ADDRESS", "https://google.com"))


if __name__ == '__main__':
    app.threaded = getenv("FLASK_THREADED", "true").lower() in ("true", "1")
    app.debug = getenv("FLASK_DEBUG", "false").lower() in ("true", "1")
    app.run(host='0.0.0.0', port=5000)
